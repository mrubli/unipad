// Copyright © 2020-2024 Martin Rubli
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

// Note that, on Windows, we must define SPDLOG_WCHAR_TO_UTF8_SUPPORT everywhere spdlog.h is
// included, so every UniPad file that uses spdlog must include this file instead of spdlog.h
// directly.
#if defined(SPDLOG_USE_STD_FORMAT)
// Our formatters are not currently made for std::format, although there's nothing inherently
// preventing them from being changed accordingly.
#error "SPDLOG_USE_STD_FORMAT is not supported."
#endif
#if defined(_WIN32)
#define SPDLOG_WCHAR_TO_UTF8_SUPPORT
#endif
#include <spdlog/spdlog.h>

// Include fmt/std.h, which defines formatters for standard types like std::filesystem::path.
#include <spdlog/fmt/std.h>

#include <wx/string.h>


void
setup_logging();


// Formatter that allows formatting the result of wxString::utf8_str().
inline
std::string
format_as(const wxScopedCharBuffer& buf)
{
	return fmt::format("{}", buf.data());
}


inline
std::string
format_as(const wxString& s)
{
	return fmt::format("{}", s.utf8_str());
}
