﻿// Copyright © 2019-2024 Martin Rubli
// SPDX-License-Identifier: BSD-2-Clause

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/clipbrd.h>
#include <wx/display.h>
#include <wx/timer.h>

#include <algorithm>
#include <array>
#include <functional>
#include <memory>
#include <stdexcept>
#include <vector>
#include <cassert>

#include "config.hpp"
#include "log.hpp"


// Globals
//

const auto AppName = L"UniPad";


// Settings
//

namespace
{

	const auto FrameBgColor				= wxColour{ 0xAA, 0xEB, 0xFF };
	const auto ButtonUnfocusedBgColor	= wxColour{ 0x404040 };
	const auto ButtonUnfocusedFgColor	= wxColour{ 0xE0E0E0 };
	const auto ButtonFocusedBgColor		= wxColour{ 0x606060 };
	const auto ButtonFocusedFgColor		= *wxWHITE;
	const auto TipBgColor				= FrameBgColor;

	const auto ButtonFont = wxFont{ 36, wxFONTFAMILY_SWISS,    wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Corbel" };
	const auto TipFont    = wxFont{ 10, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Consolas" };
	

	struct Border
	{
		int	Top{};
		int Right{};
		int Bottom{};
		int Left{};
	};

	constexpr auto Padding = Border{
		1,
		1,
		1,
		1,
	};

	struct Vector
	{
		int X{};
		int Y{};
	};

	constexpr auto ButtonSpacing = Vector{ 1, 1 };

}


// Helpers
//

namespace
{

	struct Shortcut
	{
		wxString	Text;
		int			Key;	// Untranslated key code (e.g. upper-case ASCII value or wxKeyCode)

		explicit operator bool() const { return !Text.IsEmpty(); }
	};

}


// Utility functions
//

namespace
{

	[[maybe_unused]]
	std::wostream&
	operator<<(std::wostream& os, const wxSize& s)
	{
		os << L"(" << s.GetWidth() << L", " << s.GetHeight() << L")";
		return os;
	}


	// Returns the smallest value x so that x * x ≥ n and x ≤ 16 or 0 if no such number could be
	// found.
	constexpr
	int
	sqrt_of_next_higher_square(int n)
	{
		for(int i = 1; i <= 16; i++)
		{
			if(i * i >= n)
				return i;
		}
		return 0;
	}


	template<typename T, void(*F)(T*)>
	struct Functor
	{
		void operator()(T *t) { F(t); }
	};

	void
	close_clipboard(wxClipboard *c)
	{
		if(c) { c->Close(); }
	}

	using Clippy = std::unique_ptr<wxClipboard, Functor<wxClipboard, close_clipboard>>;

	Clippy
	open_clipboard()
	{
		return Clippy{ wxTheClipboard->Open() ? wxTheClipboard : nullptr };
	}

	bool
	copy_to_clipboard(const wxString& text)
	{
		auto clippy = open_clipboard();
		if(!clippy)
		{
			spdlog::error("Unable to open clipboard.");
			return false;
		}

		if(!clippy->SetData(new wxTextDataObject(text)))
		{
			spdlog::error("Unable to set clipboard text to '{}'.", text);
			return false;
		}

		// Flush the clipboard to ensure that the content stays available after we exit.
		// This was only implemented in wxWidgets 3.1.3:
		// https://github.com/wxWidgets/wxWidgets/commit/28a84486bdf54c8f8718f0fd77312b8a28553cdd
		if(!clippy->Flush())
		{
#if !defined(__WXGTK__) || wxVERSION_NUMBER >= 3103
			spdlog::error("Unable to flush clipboard.");
			return false;
#endif
		}

		spdlog::info("Successfully copied text '{}' to clipboard.", text);
		return true;
	}


	// Depth-first traversal (post-order) of the given window and its children
	template<typename F, typename... Args>
	void
	visit_recursively(wxWindow *window, F func, Args&&... args)
	{
		for(auto&& child : window->GetChildren())
		{
			visit_recursively(child, func, std::forward<Args>(args)...);
		}
		func(window, std::forward<Args>(args)...);
	}


	bool
	read_settings()
	{
		auto result = ConfigResult::Success;
		auto error = std::wstring{};
		std::tie(result, error) = read_config_file();
		switch(result)
		{
			case ConfigResult::Success:
				break;
			case ConfigResult::FileNotFound:
				init_default_config();	// Load default settings
				write_config_file();	// Write the current settings to the settings file
				break;
			case ConfigResult::SyntaxError:
			{
				const auto response = wxMessageBox(
					L"An error has occurred while reading the configuration file:\r\n\r\n" + error + L"\r\n\r\n"
						L"Do you want to write a new configuration file containing default settings?\r\n"
						L"Selecting No will continue using default settings.",
					AppName,
					wxYES_NO | wxICON_ERROR
				);
				if(response == wxYES)
				{
					init_default_config();
					write_config_file();
				}
				// Continue with the default settings ...
				break;
			}
			case ConfigResult::ConfigError:
			{
				const auto response = wxMessageBox(
					L"Errors have occurred while reading the configuration file:\r\n\r\n" + error + L"\r\n\r\n"
						L"Do you want to write a new configuration file while preserving existing valid settings?\r\n"
						L"Selecting No will continue using a mix of valid configuration file settings and default settings.\r\n"
						L"Select Cancel to exit the application.",
					AppName,
					wxYES_NO | wxCANCEL | wxICON_ERROR
				);
				if(response == wxYES)
				{
					write_config_file();
				}
				else if(response == wxCANCEL)
				{
					return false;
				}
				// Continue with the semi-valid settings and hope for the best ...
				break;
			}
		}

		return true;
	}

}


class MainWindow
	: public wxFrame
{
private:
	wxPoint		mouseDownPos_;	// Position of the LeftDown event in MainWindow coordinates

public:
	MainWindow()
		: wxFrame(NULL, wxID_ANY, AppName, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE)
	{
		Bind(wxEVT_KEY_DOWN, &MainWindow::OnKeyDown, this);
		Bind(wxEVT_CHAR_HOOK, &MainWindow::OnCharHook, this);

		Bind(wxEVT_LEFT_DOWN, &MainWindow::OnLeftDown, this);
		Bind(wxEVT_LEFT_UP, &MainWindow::OnLeftUp, this);
		Bind(wxEVT_MOTION, &MainWindow::OnMouseMove, this);
		Bind(wxEVT_MOUSE_CAPTURE_LOST, &MainWindow::OnMouseCaptureLost, this);
	}

	void BindChildEvents()
	{
		visit_recursively(this,
			[] (wxWindow *window, MainWindow *thiz) {
				// Don't bind the MainWindow's event to OnChildLeftDown because we want to handle
				// OnLeftDown separately. (Currently the latter calls the former, so it wouldn't
				// really matter except being a little more confusing.)
				if(window != thiz)
				{
					window->Bind(wxEVT_LEFT_DOWN, &MainWindow::OnChildLeftDown, thiz);
				}
			},
			this
		);
	}

private:
	void OnKeyDown(wxKeyEvent& event)
	{
		event.Skip();
	}

	void OnCharHook(wxKeyEvent& event);

	void OnLeftDown(wxMouseEvent& event)
	{
		// Handle clicks on the MainWindow in the same way as clicks on the children
		OnChildLeftDown(event);

		// Swallow the event to avoid the main window getting a focus, leaving none of the buttons
		// with the focus.
		event.Skip(false);
	}

	void OnChildLeftDown(wxMouseEvent& event)
	{
		// If Alt is pressed while clicking the child window start dragging the window
		if(event.GetModifiers() == wxMOD_ALT)
		{
			// Capture the mouse, i.e. redirect mouse events to the MainWindow instead of the
			// child that was clicked. We revert this in OnLeftUp and OnMouseCaptureLost.
			CaptureMouse();

			const auto eventSource = static_cast<wxWindow *>(event.GetEventObject());
			const auto screenPosClicked = eventSource->ClientToScreen(event.GetPosition());
			const auto origin = GetPosition();	// MainWindow's origin

			// Save the coordinates of the clicked point relative to MainWindow's origin
			mouseDownPos_ = screenPosClicked - origin;
		}
		else
		{
			// Do nothing, i.e. pass the event on to the child window
			event.Skip();
		}
	}

	void OnLeftUp(wxMouseEvent&)
	{
		if(HasCapture())
		{
			ReleaseMouse();
		}
	}

	void OnMouseMove(wxMouseEvent& event)
	{
		// If the user is dragging the mouse then move the window with it
		if(event.Dragging() && event.LeftIsDown())
		{
			const auto screenPosCurrent = ClientToScreen(event.GetPosition());
			Move(screenPosCurrent - mouseDownPos_);
		}
	}

	void OnMouseCaptureLost(wxMouseCaptureLostEvent&)
	{
		// This event fires if our window loses the focus, e.g. when pressing Alt+Tab, while
		// dragging the window. In such a case we have to release the mouse.
		if(HasCapture())
		{
			ReleaseMouse();
		}
	}

};


enum Panel { MainPanel, MenuPanel, NumPanels };


class ButtonPanel
	: public wxPanel
{
private:
	MainWindow * const	parent_;
	const int			rows_;
	const int			cols_;

public:
	ButtonPanel(MainWindow *parent, int rows, int cols)
		: wxPanel{ parent, wxID_ANY,
			wxPoint{ Padding.Left, Padding.Top },
			parent->GetSize() - wxSize{ Padding.Left + Padding.Right, Padding.Top + Padding.Bottom }
		}
		, parent_{ parent }
		, rows_{ rows }
		, cols_{ cols }
	{
	}

	const wxSize& GetButtonSize() const
	{
		static const auto size = wxSize{
			(GetSize().GetWidth() - (cols_ - 1) * ButtonSpacing.X) / cols_,
			(GetSize().GetHeight() - (rows_ - 1) * ButtonSpacing.Y) / rows_
		};
		return size;
	}
};


class UiButton;


// Container for a UiButton.
// This wrapper is necessary on GTK because the wxStaticText we use for the shortcut tip does not
// accept a wxButton as its parent. Doing so causes an assertion in the wxWidgets GTX code to fail.
// Our workaround is to wrap every button inside a panel, which then serves as the parent for the
// button and the shortcut text.
class UiButtonWrapper
	: public wxPanel
{
	ButtonPanel *	parent_;
	UiButton *		button_{};

	UiButtonWrapper(ButtonPanel *parent)
		: wxPanel{ parent }
		, parent_{ parent }
	{}

	void SetButton(UiButton *button);

public:
	ButtonPanel *	GetButtonPanel() { return parent_; }
	UiButton * 		GetUiButton() { return button_; }

	template<typename ButtonType, typename... Args>
	static
	UiButtonWrapper *
	Make(ButtonPanel *parent, Args&&... args)
	{
		const auto wrapper = new UiButtonWrapper{ parent };
		wrapper->SetButton(new ButtonType(wrapper, std::forward<Args>(args)...));
		return wrapper;
	}
};


class UiButton
	: public wxButton
{
private:
	int			position_{ -1 };
	Shortcut	shortcut_;

public:
	UiButton(UiButtonWrapper *parent, const wxString& label, const Shortcut& shortcut = {})
		: wxButton{ parent, wxID_ANY, label, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE }
		, shortcut_{ shortcut }
	{
		const auto& size = parent->GetButtonPanel()->GetButtonSize();
		SetSizeHints(size, size);	// Set the size and minimum size

		SetFont(ButtonFont);

		CreateShortcutTip(parent);

		Bind(wxEVT_BUTTON,		&UiButton::OnClick,			this);
		Bind(wxEVT_SET_FOCUS,	&UiButton::OnSetFocus,		this);
		Bind(wxEVT_KILL_FOCUS,	&UiButton::OnKillFocus,		this);
	}

	int GetPosition() const
	{
		assert(position_ >= 0);
		return position_;
	}

	void SetPosition(int position)
	{
		position_ = position;
	}

	const Shortcut& GetShortcut() const { return shortcut_; }

	// Interface for derived classes
private:
	virtual wxColor GetUnfocusedBgColor() const { return *wxWHITE; }
	virtual wxColor GetUnfocusedFgColor() const { return *wxBLACK; }
	virtual wxColor GetFocusedBgColor()   const { return wxColour{ 0xF2F2F2 }; }
	virtual wxColor GetFocusedFgColor()   const { return GetUnfocusedFgColor(); }

	virtual void OnClick(wxCommandEvent& event) = 0;

	// Event handlers
private:
	void OnSetFocus(wxFocusEvent&);

	void OnKillFocus(wxFocusEvent&)
	{
		//dout << "OnKillFocus " << position_ << std::endl;
		UpdateStyle();
	}

	// Helpers
protected:
	void CreateShortcutTip(UiButtonWrapper *wrapper)
	{
		if(!Settings.UseAccessKeys || !shortcut_)
			return;

		constexpr auto CornerOffset = 0;

		auto tip = new wxStaticText{
			wrapper, wxID_ANY, shortcut_.Text,
			wxDefaultPosition, wxDefaultSize,
			// Note: GTK can't change the alignment after construction, so we need to set it here.
			wxALIGN_CENTRE_HORIZONTAL
		};
		tip->SetBackgroundColour(TipBgColor);
		tip->SetFont(TipFont);

		// Position the tip in the lower right corner of the button
		const auto tipMinWidth = tip->GetBestWidth(tip->GetCharHeight());
		const auto tipMinHeight = tip->GetCharHeight();
		const auto tipSize = wxSize{ std::max(tipMinWidth, tipMinHeight), std::max(tipMinWidth, tipMinHeight) };
		tip->SetSize(tipSize);
		tip->SetPosition(wrapper->GetButtonPanel()->GetButtonSize() - wxPoint{ tipSize.GetWidth() + CornerOffset, tipSize.GetHeight() + CornerOffset });
	}

	void UpdateStyle()
	{
		//dout << "UpdateStyle " << position_ << ", HasFocus: " << HasFocus() << std::endl;
		if(HasFocus())
		{
			SetBackgroundColour(GetFocusedBgColor());
			SetForegroundColour(GetFocusedFgColor());
		}
		else
		{
			SetBackgroundColour(GetUnfocusedBgColor());
			SetForegroundColour(GetUnfocusedFgColor());
		}
	}
};


void
UiButtonWrapper::SetButton(UiButton *button)
{
	button_ = button;

	// Create and assign a sizer for our wrapper. This allows the contained button to take up the
	// entire area of the wrapper.
	const auto sizer = new wxBoxSizer{ wxVERTICAL };
	SetSizer(sizer);
	sizer->Add(button_);
}


class CodeButton
	: public UiButton
{
private:
	const wxString	label_;

public:
	CodeButton(UiButtonWrapper *parent, const wxString& label, const Shortcut& shortcut = {})
		: UiButton{ parent, label, shortcut }
		, label_{ label }
	{
		UpdateStyle();
	}

	void OnClick(wxCommandEvent& event) override;
};


class ControlButton;

class ControlAction
{
private:
	const std::function<void(ControlButton&, wxCommandEvent&)>	action_;

public:
	ControlAction(std::function<void(ControlButton&, wxCommandEvent&)> action)
		: action_{ std::move(action) }
	{}

	void operator()(ControlButton& button, wxCommandEvent& event)
	{
		action_(button, event);
	}
};


struct ActivatePanelAction
	: public ControlAction
{
	explicit ActivatePanelAction(Panel panel);
};


struct ExitAppAction
	: public ControlAction
{
	ExitAppAction();
};


class ControlButton
	: public UiButton
{
private:
	ControlAction		action_;

public:
	ControlButton(UiButtonWrapper *parent, const wxString& label, ControlAction action, const Shortcut& shortcut = {})
		: UiButton{ parent, label, shortcut }
		, action_{ std::move(action) }
	{
		UpdateStyle();
	}

	wxColor GetUnfocusedBgColor() const override { return ButtonUnfocusedBgColor; }
	wxColor GetUnfocusedFgColor() const override { return ButtonUnfocusedFgColor; }
	wxColor GetFocusedBgColor()   const override { return ButtonFocusedBgColor; }
	wxColor GetFocusedFgColor()   const override { return ButtonFocusedFgColor; }

	void OnClick(wxCommandEvent& event)
	{
		action_(*this, event);
	}
};


#if 0
const auto DisplayLabelAction = ControlAction{
	[] (ControlButton& button, wxCommandEvent& event) {
		wxLogMessage(L"When I grow up I want to be a " + button.GetLabel());
	}
};
#endif


class UniPad
	: public wxApp
{
private:
	int							rows_;
	int							cols_;
	int							focusedButtonPos_{};	// zero-based

	MainWindow *				mainWindow_{};

	struct PanelInfo
	{
		ButtonPanel *				Panel{};
		std::vector<UiButtonWrapper *>		Buttons;	// Indexed by position with nullptr for spacers
	};
	std::array<PanelInfo, NumPanels>
								panels_;
	Panel						currentPanel_{ MainPanel };

	wxTimer						timer_{ this };

public:
	UniPad()
	{
		Bind(wxEVT_TIMER, &UniPad::OnTimer, this);
	}

	// Returns the total number of buttons
	int GetButtonCount() const { return rows_ * cols_; }

	// Returns the number of special buttons
	int GetSpecialButtonCount() const { return 1; }

	// Returns the number of char buttons
	int GetCharButtonCount() const { return Settings.Chars.size(); }

	enum class Direction { Up, Down, Left, Right, Home, End, PageUp, PageDown, First, Last };

	void FocusButtonRelative(Direction direction)
	{
		auto pos = focusedButtonPos_;
		switch(direction)
		{
			case Direction::Up:			pos -= cols_;										break;
			case Direction::Down:		pos += cols_;										break;
			case Direction::Left:		pos += pos % cols_ ? -1 : cols_ - 1;				break;
			case Direction::Right:		pos += pos % cols_ == cols_ - 1 ? -cols_ + 1 : 1;	break;
			case Direction::Home:		pos = (pos / cols_) * cols_;						break;
			case Direction::End:		pos = (pos / cols_) * cols_ + cols_ - 1;			break;
			case Direction::PageUp:		pos %= cols_;										break;
			case Direction::PageDown:	pos = cols_ * (rows_ - 1) + pos % cols_;			break;
			case Direction::First:		pos = 0;											break;
			case Direction::Last:		pos = GetButtonCount() - 1;							break;
		}
		FocusButton(WrapAroundPosition(pos), direction);
	}

	void UpdateFocusedButton(int position)
	{
		assert(position < GetButtonCount());
		focusedButtonPos_ = position;
	}

	void ActivatePanel(Panel panel)
	{
		const auto HideEffect = wxSHOW_EFFECT_SLIDE_TO_LEFT;
		const auto ShowEffect = wxSHOW_EFFECT_SLIDE_TO_RIGHT;
		const auto DurationMs = 150;

		if(panel < 0 || panel >= NumPanels)
			throw std::logic_error{ "Invalid panel ID given" };

		// Get a list of panels to deactivate and the (single) panel to activate
		auto toHide = std::vector<ButtonPanel *>{};
		for(auto&& p : panels_)
		{
			if(p.Panel->IsShown())
				toHide.push_back(p.Panel);
		}
		auto toShow = panels_[panel].Panel;
		const auto useEffect = toHide.size() == 1;

		// Hide panels to be deactivated (use animation iif we need to hide a single panel)
		for(auto&& p : toHide)
		{
			if(useEffect)
			{
				p->HideWithEffect(HideEffect, DurationMs);
			}
			else
			{
				p->Hide();
			}
		}

		// Show the panel to be activated
		if(useEffect)
		{
			toShow->ShowWithEffect(ShowEffect, DurationMs);
		}
		else
		{
			toShow->Show();
		}

		currentPanel_ = panel;

		// Re-focus the current position to redraw the focus but also in case the current position
		// is empty in the new panel.
		FocusButton(focusedButtonPos_, Direction::Up);
	}

	UiButton * FindButtonByShortcut(int key)
	{
		auto pos = 0;
		for(const auto& button : panels_[currentPanel_].Buttons)
		{
			if(button && button->GetUiButton()->GetShortcut().Key == key)
			{
				return button->GetUiButton();
			}
			pos++;
		}
		return nullptr;	// No shortcut matched the key code
	}

	void Quit(bool storeClipboard = false)
	{
		const auto delayedExit =
#ifdef WIN32
			false				// On Windows we never need delayed exit
#else
			storeClipboard		// On X11 we need to wait until the fairies have etched the
								// clipboard contents onto unicorn ivory panels. If we exit
								// immediately the clipboard contents are lost.
#endif
		;

		if(delayedExit)
		{
			// Hide the window immediately and close it with a delay
			mainWindow_->Hide();
			timer_.StartOnce(1000);		// This is how long it takes _my_ fairies
		}
		else
		{
			// Close the window immediately
			mainWindow_->Close();
		}		
	}

private:
	bool InitSettings()
	{
		// Read the settings
		const auto success = read_settings();
		if(!success)
			return false;

		// Set the number of rows/columns to be the smallest square that holds all buttons
		rows_ = sqrt_of_next_higher_square(Settings.Chars.size() + 1);
		cols_ = rows_;

		return true;
	}

	// Adds different sets of buttons to a panel and a grid at the same time.
	// The 'buttons' vector is modified to contain button pointers at their corresponding positions
	// and null pointers at the empty positions (spacers).
	void AddButtonsToGrid(ButtonPanel *panel, wxGridSizer *grid,
						  std::vector<UiButtonWrapper *>& buttons,
						  const std::vector<UiButtonWrapper *>& lastButtons)
	{
		const auto NumPositions = GetButtonCount();

		const auto numSpacers = NumPositions - static_cast<int>(lastButtons.size() + buttons.size());
		assert(numSpacers >= 0);

		auto pos = 0;

		// Add the main buttons first
		for(const auto& wrapper : buttons)
		{
			grid->Add(wrapper);
			wrapper->GetUiButton()->SetPosition(pos++);
		}

		// Add just enough spacers
		for(int i = 0; i < numSpacers; i++)
		{
			grid->AddSpacer(0);	// Spacer size does not matter for grids
			pos++;
		}

		// Add the buttons that should appear last
		auto i = buttons.size() + numSpacers;
		buttons.resize(NumPositions);
		for(const auto& wrapper : lastButtons)
		{
			buttons[i++] = wrapper;
			grid->Add(wrapper);
			wrapper->GetUiButton()->SetPosition(pos++);
		}

		assert(pos == NumPositions);
	}

	void CreateMainPanel(MainWindow *frame)
	{
		auto& pi = panels_[MainPanel];

		// Create a panel, so that tab navigation works
		pi.Panel = new ButtonPanel{ frame, rows_, cols_ };

		// Create a grid for the buttons
		auto grid = new wxGridSizer{ rows_, cols_, ButtonSpacing.Y, ButtonSpacing.X };

		// Create code buttons and menu button
		const auto NumButtons = GetCharButtonCount();
		for(int pos = 0; pos < NumButtons; pos++)
		{
			auto button = UiButtonWrapper::Make<CodeButton>(pi.Panel, Settings.Chars[pos], GetButtonShortcut(pos));
			pi.Buttons.push_back(button);
		}
		auto menuButton = UiButtonWrapper::Make<ControlButton>(pi.Panel, L"☰", ActivatePanelAction{ MenuPanel } /*, { "/", '/' }*/);

		// Add all buttons to the grid, menu button last
		AddButtonsToGrid(pi.Panel, grid, pi.Buttons, { menuButton });

		// Do the layout.
		// Note that calling
		//   panel->SetSizerAndFit(grid)
		// here breaks the layout as the grid ends up with a (0, 0) size.
		pi.Panel->SetSizer(grid);
		pi.Panel->Layout();

#if 0
		dout << "Frame: " << frame->GetSize() << std::endl;
		dout << "Panel: " << panel->GetSize() << std::endl;
		dout << "Grid:  " << grid->GetSize() << L", Min: " << grid->GetMinSize() << std::endl;
#endif
	}

	void CreateMenuPanel(MainWindow *frame)
	{
		auto& pi = panels_[MenuPanel];

		// Create a menu panel
		pi.Panel = new ButtonPanel{ frame, rows_, cols_ };
		auto grid = new wxGridSizer{ rows_, cols_, ButtonSpacing.Y, ButtonSpacing.X };

		// Create menu buttons and return button
		pi.Buttons.push_back(UiButtonWrapper::Make<ControlButton>(pi.Panel, L"❌", ExitAppAction{}));
		//pi.Buttons.push_back(UiButtonWrapper::Make<ControlButton>(pi.Panel, L"🖉", ExitAppAction{}));
		auto returnWrapper = UiButtonWrapper::Make<ControlButton>(pi.Panel, L"↩", ActivatePanelAction{ MainPanel });

		// Add all buttons to the grid, return button last
		AddButtonsToGrid(pi.Panel, grid, pi.Buttons, { returnWrapper });

		// Do the layout
		pi.Panel->SetSizer(grid);
		pi.Panel->Layout();
	}

	static Shortcut GetButtonShortcut(int pos)
	{
		auto letter = '\0';
		if(pos >= 0 && pos < 9)
		{
			letter = '1' + pos;
		}
		else if(pos == 9)
		{
			letter = '0';
		}
		else if(pos >= 10 && pos < (10 + 26))
		{
			letter = 'A' + pos - 10;
		}
		else
		{
			return {};	// We're out
		}
		return Shortcut{ letter, letter };
	}

	void FocusButton(int position, Direction direction)
	{
		assert(position < GetButtonCount());

		auto& pi = panels_[currentPanel_];

		// If the desired position happens to be a spacer focus the previous/next element
		while(pi.Buttons[position] == nullptr || !pi.Buttons[position]->IsShown())
		{
			const int dir = [direction] {
				switch(direction)
				{
					case Direction::Left:
					case Direction::Up:
					case Direction::Home:
					case Direction::PageUp:
					case Direction::Last:
						return -1;
					case Direction::Down:
					case Direction::Right:
					case Direction::End:
					case Direction::PageDown:
					case Direction::First:
						return 1;
				}
				throw std::logic_error{ "Invalid direction encountered in FocusButton()" };
			}();
			position += dir;
		}

		pi.Buttons[position]->SetFocus();
	}

	int WrapAroundPosition(int position) const
	{
		const auto NumButtons = GetButtonCount();
		return (position + NumButtons) % NumButtons;
	}

private:
	// The main() of a wxWidgets application
	bool OnInit() override
	{
		// Initialize logging
		setup_logging();

		// Start by reading the settings
		if(const auto success = InitSettings(); !success)
		{
			spdlog::error("Failed to initialize settings. Exiting ...");
			return false;	// Exit the app
		}

		MainWindow *frame = new MainWindow();

		frame->SetBackgroundColour(FrameBgColor);

		// Size and position the main window
		{
			wxDisplay display{ 0u };
			const auto smaller = std::min(display.GetGeometry().width, display.GetGeometry().height);

			// Make the width and height so that they can be cleanly divided into columns/rows
			// after subtracting the whitespace (margin and grid lines).
			const auto whitespaceH = (Padding.Left + Padding.Right) + (cols_ - 1) * ButtonSpacing.X;
			const auto whitespaceV = (Padding.Top + Padding.Bottom) + (rows_ - 1) * ButtonSpacing.Y;
			const auto width   = (smaller / 2) / cols_ * cols_ + whitespaceH;
			const auto height  = (smaller / 2) / rows_ * rows_ + whitespaceV;

			const auto rect = wxRect{
				display.GetGeometry().width / 2 - smaller / 4,		// Left
				display.GetGeometry().height / 2 - smaller / 4,		// Top
				width,												// Width
				height												// Height
			};
			frame->SetSize(rect);
		}

		// Create the panels
		CreateMainPanel(frame);
		CreateMenuPanel(frame);
		ActivatePanel(MainPanel);

		// Recursively bind the frame's child window events.
		// This must be done after all relevant child windows have been added.
		frame->BindChildEvents();

		// Display the main window
		frame->Show(true);

		mainWindow_ = frame;
		return true;
	}

	void OnTimer(wxTimerEvent&)
	{
		spdlog::info("Timer expired. Closing main window.");
		mainWindow_->Close();
	}
};


wxIMPLEMENT_APP(UniPad);


void
MainWindow::OnCharHook(wxKeyEvent& event)
{
	switch(event.GetKeyCode())
	{
		case WXK_ESCAPE:
			wxGetApp().Quit();
			break;
		case WXK_UP:
			wxGetApp().FocusButtonRelative(UniPad::Direction::Up);
			break;
		case WXK_DOWN:
			wxGetApp().FocusButtonRelative(UniPad::Direction::Down);
			break;
		case WXK_LEFT:
			wxGetApp().FocusButtonRelative(UniPad::Direction::Left);
			break;
		case WXK_RIGHT:
			wxGetApp().FocusButtonRelative(UniPad::Direction::Right);
			break;
		case WXK_HOME:
			if(event.GetModifiers() == wxMOD_NONE)
				wxGetApp().FocusButtonRelative(UniPad::Direction::Home);
			else if(event.GetModifiers() == wxMOD_CONTROL)
				wxGetApp().FocusButtonRelative(UniPad::Direction::First);
			break;
		case WXK_END:
			if(event.GetModifiers() == wxMOD_NONE)
				wxGetApp().FocusButtonRelative(UniPad::Direction::End);
			else if(event.GetModifiers() == wxMOD_CONTROL)
				wxGetApp().FocusButtonRelative(UniPad::Direction::Last);
			break;
		case WXK_PAGEUP:
			wxGetApp().FocusButtonRelative(UniPad::Direction::PageUp);
			break;
		case WXK_PAGEDOWN:
			wxGetApp().FocusButtonRelative(UniPad::Direction::PageDown);
			break;
		default:
		{
			if(Settings.UseAccessKeys)
			{
				// For other key codes check if the shortcut key of any of the current UI buttons matches
				if(const auto button = wxGetApp().FindButtonByShortcut(event.GetKeyCode()); button)
				{
					// If a button matched focus and press it
					button->SetFocus();
					auto e = wxCommandEvent{ wxEVT_BUTTON };
					button->Command(e);
				}
				else
				{
					// Don't handle the event if there was no match for the key code
					event.Skip();
				}
			}
			break;
		}
	}
}


void
UiButton::OnSetFocus(wxFocusEvent&)
{
	//dout << "OnSetFocus " << position_ << std::endl;
	UpdateStyle();
	wxGetApp().UpdateFocusedButton(GetPosition());
}


void
CodeButton::OnClick(wxCommandEvent& event)
{
	//wxLogMessage(L"Hello world from code button at position %d: %s", position_, label_.c_str());
	copy_to_clipboard(label_);

	// TODO confirmation animation => see shaped.cpp sample (blend button(s) into a clipboard icon?)

	// Reverse the close-on-copy setting if Shift is held down
	auto closeOnCopy = Settings.CloseOnCopy;
	if(wxGetKeyState(WXK_SHIFT))
		closeOnCopy = !closeOnCopy;

	if(closeOnCopy)
	{
		wxGetApp().Quit(true);
	}
}


ActivatePanelAction::ActivatePanelAction(Panel panel)
	: ControlAction{
		[panel] (ControlButton& button, wxCommandEvent& event) {
			wxGetApp().ActivatePanel(panel);
		}
	}
{}

ExitAppAction::ExitAppAction()
	: ControlAction{
		[] (ControlButton&, wxCommandEvent&) {
			wxGetApp().Quit();
		}
	}
{}
