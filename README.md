﻿# UniPad

## Introduction

UniPad provides quick access to commonly used Unicode characters.

When launched it displays a square window with one button per character.
Pressing a button copies the corresponding character to the clipboard.

![UniPad screenshot](doc/screenshot.png)

UniPad uses [wxWidgets](https://wxwidgets.org/) as a UI framework, which can be statically linked.
It has currently been tested on Windows and Linux but it can probably be made to compile on other platforms as well.


## Features

- Configurable list of characters.
- Quick access keys (0-9 and A-Z) for characters.
- Single executable


## Configuration file syntax

Configuration files use the [TOML v0.4.0](https://github.com/toml-lang/toml/blob/master/versions/en/toml-v0.4.0.md) format.
[tinytoml](https://github.com/mayah/tinytoml) is used for parsing.

Sample configuration file:

```
[settings]
Chars = [
	"…",
	"–",
	"←",
	"↑",
	"→",
	"↓",
	"↔",
	"↗",
	"↘",
	"⇒",
	"①",
	"②",
	"③",
	"¹",
	"²",
	"³",
	"≠",
	"≈",
	"≤",
	"≥",
	"「」",
	"℃",
	"µ",
]
CloseOnCopy = true
UseAccessKeys = true
```

### Configuration options

`Chars`: An array of strings to be used as "characters". Multi-character strings are allowed.

`CloseOnCopy`: Close UniPad after a character has been selected and copied to the clipboard.
The Shift key may be pressed while clicking a button or pressing an access key to temporarily invert this setting.
Default: `true`

`UseAccessKeys`: Enable display and use of quick access keys for characters. Default: `true`


## Dependencies

- [wxWidgets](https://wxwidgets.org/) (currently using version 3.x, on Debian install `libwxgtk3.2-dev`)

- [tinytoml](https://github.com/mayah/tinytoml) (currently using v0.4)
