﻿// Copyright © 2019-2024 Martin Rubli
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <iomanip>
#include <ostream>
#include <tuple>
#include <vector>

#include <spdlog/fmt/fmt.h>

#include <wx/string.h>

#include "log.hpp"


struct ProgramSettings
{
	std::vector<wxString>	Chars = {
		L"α",
		L"β",
		L"γ",
		L"∫",
		L"∮",
		L"⊗",
	};

	bool					CloseOnCopy{ true };
	bool					UseAccessKeys{ true };
};

inline
auto
format_as(const ProgramSettings &ps)
{
	auto buf = fmt::memory_buffer{};
	fmt::format_to(std::back_inserter(buf),
		"{{ Chars: [ {} ]",
		fmt::join(ps.Chars, ", "));
	fmt::format_to(std::back_inserter(buf),
		", CloseOnCopy: {}, UseAccessKeys: {} }}",
		ps.CloseOnCopy, ps.UseAccessKeys);
	return fmt::to_string(buf);
}


extern ProgramSettings Settings;


void
init_default_config();

enum class ConfigResult { Success, FileNotFound, SyntaxError, ConfigError };

// Returns a { result, errorString } tuple where errorString is a human-readable error message and
// result is one of the following:
// - Success if no error occurred.
// - FileNotFound if the config file cannot be opened. (The current settings are not modified.)
// - SyntaxError if the config file cannot be parsed. (The current settings are not modified.)
// - ConfigError if at least one setting was erroneous. (Default settings were loaded and overridden
//   with whatever settings could be loaded from the config file.)
std::tuple<ConfigResult, wxString>
read_config_file();

bool
write_config_file();
