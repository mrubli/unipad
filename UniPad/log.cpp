// Copyright © 2020-2024 Martin Rubli
// SPDX-License-Identifier: BSD-2-Clause

#include "log.hpp"

#if defined(_WIN32)
#include <spdlog/sinks/msvc_sink.h>
#endif


void
setup_logging()
{
#if defined(_WIN32)
	// Note: We pass 'check_debugger_present = false' here, so that logging messages show up in DebugView.
	auto msvc_sink = std::make_shared<spdlog::sinks::msvc_sink_st>(false);
	auto logger = std::make_shared<spdlog::logger>(
		"custom_sink",
		spdlog::sinks_init_list{
			msvc_sink,
		}
	);
	spdlog::set_default_logger(logger);
#endif
}
