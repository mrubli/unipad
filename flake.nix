{
	description = "UniPad";

	inputs = {
		nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
	};

	outputs = { self, nixpkgs, ... }: let
		system = "x86_64-linux";
		pkgs = import nixpkgs {
			inherit system;
		};
	in {
		# Dev shell for 'nix develop'
		devShells."${system}" = {
			default = pkgs.mkShell {
				packages = with pkgs; [
					meson
					ninja
					wxGTK32
				];
				shellHook = ''
					echo "Meson version: $(meson --version)"
				'';
			};
		};

		# Build package
		packages."${system}" = {
			unipad = pkgs.stdenv.mkDerivation {
				pname = "unipad";    # gives a name of "${pname}-${version}"
				version = "0.1.0";
				src = self;
				nativeBuildInputs = with pkgs; [
					meson
					cmake	# allows Meson to discover spdlog
					ninja
				];
				buildInputs = with pkgs; [
					wxGTK32
					spdlog
				];
				meta = with pkgs.lib; {
					description = "Visual selector for favorite Unicode characters";
					homepage = "https://gitlab.com/mrubli/unipad";
					license = licenses.bsd2;
				};
			};
			default = self.packages."${system}".unipad;
		};
	};
}
