﻿// Copyright © 2019-2024 Martin Rubli
// SPDX-License-Identifier: BSD-2-Clause

#include <filesystem>
#include <ranges>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4996)
#endif
#include <toml/toml.h>
#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <wx/app.h>
#include <wx/stdpaths.h>

#include "log.hpp"

#include "config.hpp"


ProgramSettings Settings = {};


namespace
{

const auto SettingsFile = wxString{ L"UniPad.toml" };


std::filesystem::path
to_path(const wxString& path)
{
	static_assert(std::is_same_v<char, std::filesystem::path::value_type>
	           || std::is_same_v<wchar_t, std::filesystem::path::value_type>);
	if constexpr (std::is_same_v<char, std::filesystem::path::value_type>)
	{
		return { path.ToUTF8().data() };
	}
	else if constexpr (std::is_same_v<wchar_t, std::filesystem::path::value_type>)
	{
		return { path.wc_str() };
	}
}


std::string
to_utf8(const wxString& s)
{
	return s.utf8_str().data();
}


template<typename T>
std::tuple<bool, wxString>
read_setting(const toml::Value& table, const wxString& name, T& value)
{
	spdlog::info("Reading setting '{}'.", name);

	const auto* v = table.find(to_utf8(name));
	if(v && v->is<T>())
	{
		value = v->as<T>();
		return { true, {} };
	}
	else
	{
		const auto s = fmt::format("Setting '{}' not found or its value has the wrong type.", name);
		spdlog::info("{}", s);
		return { false, s };
	}
}


// Try to read whatever settings we can and collects errors as we go.
void
read_config_table(const toml::Table& table, ProgramSettings& settings,
                  std::vector<wxString>& errors, const wxString& tableName)
{
	spdlog::info("Reading configuration section '{}'.", tableName);

	// Go over all the table's values first while skipping subtables
	for(auto&&[key, value] : table)
	{
		auto success = false;
		auto error = wxString{};
		if(key == "Chars")
		{
			auto values = std::vector<std::string>{};
			std::tie(success, error) = read_setting(table, key, values);
			if(!success)
			{
				errors.push_back(error);
			}
			else
			{
				settings.Chars.clear();
				std::transform(std::cbegin(values), std::cend(values),
								std::back_inserter(settings.Chars),
								[] (const auto& s) { return wxString::FromUTF8(s.c_str()); });
			}
		}
		else if(key == "CloseOnCopy")
		{
			std::tie(success, error) = read_setting(table, key, settings.CloseOnCopy);
		}
		else if(key == "UseAccessKeys")
		{
			std::tie(success, error) = read_setting(table, key, settings.UseAccessKeys);
		}
		else
		{
			error = L"Unsupported setting '" + key + L"' encountered.";
		}

		if(!success)
		{
			errors.push_back(error);
		}
	}
}


template<typename T>
bool
write_setting(std::ostream& os, const char *name, const T& value)
{
	os << name << " = " << toml::Value{ value } << std::endl;
	return true;
}

bool
write_setting(std::ostream& os, const char *name, const std::vector<wxString>& values)
{
	os << name << " = [\n";
	for(auto&& v : values)
		os << "\t\"" << v.ToUTF8() << "\",\n";
	os << "]" << std::endl;
	return true;
}


std::tuple<ConfigResult, wxString>
read_config_file(std::filesystem::path configFile)
{
	spdlog::info("Reading settings file {} ...", configFile);

	// Open the file
	auto is = std::ifstream{ configFile, std::ios_base::in };
	if(!is)
	{
		const auto s = fmt::format("Failed to open settings file {}.", configFile);
		spdlog::error("{}", s);
		return { ConfigResult::FileNotFound, s };
	}

	// Parse it using the TOML parser.
	// Note that the root value is always of type TABLE_TYPE, even if the file is empty or contains no tables.
	auto pr = toml::parse(is);
	if(!pr.valid() || pr.value.type() != toml::Value::TABLE_TYPE)
	{
		const auto s = fmt::format("Failed to parse settings file {}: {}", configFile, pr.errorReason);
		spdlog::error("{}", s);
		return { ConfigResult::SyntaxError, s };
	}
	
	const auto *settingsTable = pr.value.findChild("settings");
	if(!settingsTable)
	{
		const auto s = "Settings file is missing the [settings] section.";
		spdlog::error("{}", s);
		return { ConfigResult::SyntaxError, s };
	}

	{
		// Start off with the default settings
		auto newSettings = ProgramSettings{};

		auto errors = std::vector<wxString>{};
		if(settingsTable)
		{
			read_config_table(settingsTable->as<toml::Table>(), newSettings, errors, "settings");
		}

		// Use the settings we just read (no matter complete or not)
		Settings = newSettings;

		// Return an error if there were any errors while reading settings
		if(!errors.empty())
		{
			std::wstringstream ss;
			for(auto i = 0u; i < errors.size(); i++)
			{
				if(i > 0)
					ss << L"\r\n";
				ss << L"  ▪  " << errors[i];
			}
			return make_pair(ConfigResult::ConfigError, ss.str());
		}
	}

	spdlog::info("Successfully read settings file {}\nSettings: {}", configFile, Settings);
	return { ConfigResult::Success, {} };
}


bool
write_config_file(std::filesystem::path configFile)
{
	spdlog::info("Writing settings file {} ...", configFile);

	auto os = std::ofstream{ configFile, std::ios_base::out | std::ios_base::trunc };
	if(!os)
	{
		spdlog::error("Failed to write settings file {}.", configFile);
		return false;
	}

	os << "[settings]\n";

	write_setting(os, "Chars",			Settings.Chars);
	write_setting(os, "CloseOnCopy",	Settings.CloseOnCopy);
	write_setting(os, "UseAccessKeys",	Settings.UseAccessKeys);

	spdlog::info("Successfully wrote settings file {}.", configFile);
	return true;
}


std::filesystem::path
get_config_directory()
{
	// Starting from wxWidgets 3.3, wxStandardPaths::GetUserDir(Dir_Config) returns:
	//   C:\Users\<username>\AppData\Roaming on Windows
	//   ~/.config on Linux
	// wxStandardPaths::AppendAppInfo() can then be called on that directory to get the complete
	// config directory path. Unfortunately, in earlier versions there is no such functionality,
	// so we have to do it manually.
	return
#ifdef WIN32
		to_path(wxStandardPaths::Get().GetUserDataDir())
#else
		// Note: GetUserConfigDir() returns the home directory on Linux, not ~/.config!
		to_path(wxStandardPaths::Get().GetUserConfigDir()) / L".config" / wxTheApp->GetAppName().ToStdWstring()
#endif
	;
}


std::vector<std::filesystem::path>
get_candidate_config_file_paths()
{
	const auto configFileName = to_path(SettingsFile);
	const auto basePaths = {
		std::filesystem::path{ "." },										// Current working directory
		get_config_directory(),												// User's config directory
		to_path(wxStandardPaths::Get().GetExecutablePath()).parent_path(),	// Executable directory
	};
	return basePaths
		| std::views::transform([&] (const auto& basePath) { return basePath / configFileName; })
		| std::ranges::to<std::vector>();
}


std::filesystem::path
find_config_file()
{
	const auto configPaths = get_candidate_config_file_paths();

	// Look for the first config file that exists among the candidates but log all of them
	auto chosenConfigPath = std::filesystem::path{};
	for (const auto& configPath : configPaths)
	{
		const auto exists = std::filesystem::exists(configPath);
		spdlog::info("Candidate config file: {}{}", configPath, (exists ? " (found)" : ""));
		if(exists && chosenConfigPath.empty())
		{
			chosenConfigPath = configPath;
		}
	}

	return chosenConfigPath;
}


}


void
init_default_config()
{
	spdlog::info("Using default settings.");
	Settings = {};
}


std::tuple<ConfigResult, wxString>
read_config_file()
{
	const auto configFile = find_config_file();
	if (configFile.empty())
	{
		return { ConfigResult::FileNotFound, "No config file found." };
	}

	return read_config_file(configFile);
}


bool
write_config_file()
{
	auto configFile = find_config_file();
	if (configFile.empty())
	{
		// TODO pick a good default
	}

	return write_config_file(configFile);
}
